import messages from "./locale";

export default {
  target: "static",
  ssr: true,
  generate: {
    fallback: "404.vue",
    routes: [
      "/registration/telegram",
      "/registration/instagram",
      "/registration/facebook",
      "/registration/youtube",
      "/registration/tiktok",
      "/registration/PR",
    ],
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Cambridge Learning Center",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.png",
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~assets/fonts/Montserrat/stylesheet.css",
    "~/assets/icomoon/style.css",
    "swiper/css/swiper.css",
    "~/assets/styles/main.scss",

    // 'swiper/css/swiper.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/VueAwesomeSwiper", mode: "client" },
    // { src: "~/plugins/vue-pdf.js", ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "nuxt-i18n", "vue-toastification/nuxt"],

  toast: {
    // Vue Toastification plugin options
    timeout: 2000,
    closeOnClick: false,
  },
  i18n: {
    locales: [
      {
        code: "uz",
        name: "O’zbekcha",
      },

      {
        code: "ru",
        name: "Русский",
      },
      {
        code: "en",
        name: "English",
      },
    ],
    strategy: "prefix_except_default",
    defaultLocale: "en",
    vueI18n: {
      fallBackLocale: "uz",
      messages: messages,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
