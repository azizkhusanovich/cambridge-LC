export const state = () => ({
  footer: [
    {
      en: "Why Cambridge",
      ru: "Почему именно Cambridge",
      uz: "Nima uchun Cambridge",
      link: "#choose",
    },
    {
      en: "courses",
      ru: "Курсы",
      uz: "Kurslar",
      link: "#courses",
    },
    {
      en: "Our students",
      ru: "Наши студенты",
      uz: "Bizning o’quvchilarimiz",
      link: "#students",
    },
    {
      en: "Hybrid Platform",
      ru: "Гибридная платформа",
      uz: "Gibrid platforma",
      link: "#platform",
    },
    {
      en: "Our Team",
      ru: "Наша команда",
      uz: "Bizning Jamoamiz",
      link: "#team",
    },
  ],
});
// getters
export const getters = {
  footer(state) {
    return state.footer;
  },
};

// mutators
export const mutations = {};

// actions
export const actions = {};
