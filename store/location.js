export const state = () => ({
  location: [
    {
      img: "/img/background/Drujba.jpg",
      name: "Cambridge Drujba",
      address: {
        en: `Chilanzar District, "Xalqalar Do'stligi" metro station`,
        ru: `Чиланзарский район, станция метро «Xalqalar Do'stligi»`,
        uz: `Chilonzor tumani, “Xalqalar Do'stligi” metro bekati`,
      },
      phone: "+998951447901",
      telegram: "@cambridge_drujba_admin",
      map_link: "https://g.page/cambridge_drujba?share",
      phone_link: "tel:+998951447901",
      telegram_link: "https://t.me/cambridge_drujba_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23975.436543318712!2d69.210376315625!3d41.310395799999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8b14934874dd%3A0x6e34e3f72c8d68d7!2sCambridge%20Learning%20Centre%20-%20Drujba!5e0!3m2!1sru!2s!4v1661326066243!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://g.page/cambridge_drujba?share",
        yandex: "https://yandex.uz/maps/-/CCUyJTWL3C",
      },
    },
    {
      img: "/img/background/Darhan.jpg",
      name: "Cambridge Darhan",
      address: {
        en: "Mirzo-Ulugbek district, Building 3, near Metro station Hamid Olimjon",
        ru: "Мирзо-Улугбекский район, дом 3, возле метро Хамид Олимжон",
        uz: "Mirzo Ulug'bek tumani, 3-bino, Hamid Olimjon metrosi yaqinida",
      },
      phone: "+998951457901",
      telegram: "@cambridge_darxon_admin",
      map_link: "https://goo.gl/maps/Pswv68Dg4Q2V7GLz9",
      phone_link: "tel:+998951457901",
      telegram_link: "https://t.me/cambridge_darxon_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23971.75656035669!2d69.2719115081887!3d41.3204015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38aef5d7a4273f53%3A0xd110aa42036e7dc4!2sCAMBRIDGE%20Learning%20Centre%20-%20Darhan!5e0!3m2!1sru!2s!4v1661325933707!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://goo.gl/maps/qJGdv7w4ziTy5RMU6",
        yandex: "https://yandex.uz/maps/-/CCUyJTW1SB",
      },
    },
    {
      img: "/img/background/Tinchlik.jpg",
      name: "Cambridge Tinchlik",
      address: {
        en: "Shaykhontohur District, Beruniy st., 35A near Metro station Tinchlik",
        ru: "Шайхонтохурский район, улица Беруний, дом 35А возле метро Тинчлик",
        uz: "Shayxontohur tumani, Beruniy ko‘chasi, 35A, Tinchlik metro bekati yaqinida",
      },
      phone: "+998953417901",
      telegram: "@cambridge_tinchlik_admin",
      map_link: "https://goo.gl/maps/qmxTxV5K5WXnnsP4A",
      phone_link: "tel:+998953417901",
      telegram_link: "https://t.me/cambridge_tinchlik_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23966.447788319132!2d69.18255051562498!3d41.33483229999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8d34e13914fd%3A0xf87e73c966701c27!2sCAMBRIDGE%20Learning%20Centre%20%E2%80%94%20Tinchlik!5e0!3m2!1sru!2s!4v1661326122676!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://goo.gl/maps/qzfSfRFQQ2qHjiNZ7",
        yandex: "https://yandex.uz/maps/-/CCUyJTWlLA",
      },
    },
    {
      img: "/img/background/Oybek.jpg",
      name: "Cambridge Oybek",
      address: {
        en: "Mirabad District, Mirabad st., 2/1, near Metro station Oybek.",
        ru: 'Мирабадский р-н, ул. Мирабадская, д. 2/1, рядом с метро "Ойбек".',
        uz: "Mirobod tumani, Mirobod ko'chasi, 2/1, Oybek metro bekati yaqinida.",
      },
      phone: "+998951427901",
      telegram: "@cambridge_oybek_admin",
      map_link: "https://g.page/cambridge_oybek?share",
      phone_link: "tel:+998951427901",
      telegram_link: "https://t.me/cambridge_oybek_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23980.726710714716!2d69.23476781562496!3d41.2960086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8b57405ea7e5%3A0xec992172d5677900!2sCAMBRIDGE%20Learning%20Centre%20-%20Oybek!5e0!3m2!1sru!2s!4v1661326025360!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://g.page/cambridge_oybek?share",
        yandex: "https://yandex.uz/maps/-/CCUyJTW2dD",
      },
    },
    {
      img: "/img/background/Sergeli.jpg",
      name: "Cambridge Sergeli",
      address: {
        en: "Sergeli District, New Sergeli 11/9 next to Credit Asia Sergeli.",
        ru: "Сергелийский район, Новый Сергели 11/9 рядом с Кредит Азия Сергели.",
        uz: "Sergeli tumani, Yangi Sergeli 11/9, Credit Asia Sergeli yonida.",
      },
      phone: "+998953427901",
      telegram: "@cambridge_sergeli_admin",
      map_link: "https://goo.gl/maps/WrAve1pypZDBxYWw6",
      phone_link: "tel:+998953427901",
      telegram_link: "https://t.me/cambridge_sergeli_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3001.0133577443685!2d69.22158111481366!3d41.22147937927967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae614bdb016189%3A0xbf8be2fe82c35dea!2sCAMBRIDGE%20Learning%20Centre%20-%20Sergeli!5e0!3m2!1sru!2s!4v1663060492054!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://goo.gl/maps/rUCgZy7om8sY7zdP8",
        yandex: "https://yandex.uz/maps/-/CCUyJTcYxA",
      },
    },
    {
      img: "/img/background/Novza.jpg",
      name: "Cambridge Novza",
      address: {
        en: "Chilanzar District, Muqimiy Street, 11/9, the second floor of “Makro” department store.",
        ru: "Чиланзарский район, улица Мукими, дом 11/9 Супермаркет «Макро» 2 этаж.",
        uz: "Chilonzor tumani, Muqimiy ko‘chasi, 11/9 “Makro” supermarketi 2-qavat.",
      },
      phone: "+998951967901",
      telegram: "@cambridge_novza_admin",
      map_link: "https://goo.gl/maps/5KDYqaFGwHjdBHit7",
      phone_link: "tel:+998951967901",
      telegram_link: "https://t.me/cambridge_novza_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d95929.85886880559!2d69.09188036250002!3d41.29128099999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8bd5f09912b7%3A0x8d57d2c0e09527bf!2sCAMBRIDGE%20Learning%20Centre%20-%20Novza%20(XAMZA)!5e0!3m2!1sru!2s!4v1663060574845!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://maps.google.com/?cid=10184840808165091263&entry=gps",
        yandex: "https://yandex.com/maps/-/CCUFaPs8gB",
      },
    },
    {
      img: "/img/background/YunusAbad.jpg",
      name: "Cambridge Yunusabad",
      address: {
        en: "Yunusabad District, “Stroy Center” mall near the Yunusabad roundabout.",
        ru: "Юнусабадский район, ТРЦ “Строй Центр” на круге Юнусабад",
        uz: "Yunusobod tumani, “Stroy centre” savdo markazi Yunusobod doirasi.",
      },
      phone: "+998953407901",
      telegram: "@cambridge_yunusobod_admin",
      map_link: "https://goo.gl/maps/d6kBAcEae2y71tf59",
      phone_link: "tel:+998953407901",
      telegram_link: "https://t.me/cambridge_yunusobod_admin",
      iframe: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2994.328152192329!2d69.28414301481953!3d41.36695467926604!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38aef3538199cd9b%3A0x13422e77d307b8e3!2sCAMBRIDGE%20Learning%20Centre%20-%20Yunusobod!5e0!3m2!1sru!2s!4v1663060606554!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`,
      location: {
        google: "https://goo.gl/maps/RZBTgQB4uAjxRtWh6",
        yandex: "https://yandex.uz/maps/-/CCUyJTWsSB",
      },
    },
  ],
});

// getters
export const getters = {
  location(state) {
    return state.location;
  },
};

// mutators
export const mutations = {};

// actions
export const actions = {};
