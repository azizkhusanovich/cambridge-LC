export default [
  {
    image: require("~/assets/images/teachers/Mr.Mirzokhidbek1.png"),
    name: {
      uz: "Mr. Abduraxmonov",
      ru: "Mr. Abduraxmonov",
      en: "Mr. Abduraxmonov",
    },
    experience: {
      uz: `6 YEARS`,
      ru: `6 YEARS`,
      en: `6 YEARS`,
    },
    desc: {
      uz: "A hardworking teacher with an intense passion for educating, and an ambition to pursue further education in Language Teaching. Being strict, always tries to give the best to students, expecting the best in return.",
      ru: "A hardworking teacher with an intense passion for educating, and an ambition to pursue further education in Language Teaching. Being strict, always tries to give the best to students, expecting the best in return.",
      en: "A hardworking teacher with an intense passion for educating, and an ambition to pursue further education in Language Teaching. Being strict, always tries to give the best to students, expecting the best in return.",
    },
    score: "8.5",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Mr.Abdulboriy1.png"),
    name: {
      uz: "Mr. Abdulkhamidov",
      ru: "Mr. Abdulkhamidov",
      en: "Mr. Abdulkhamidov",
    },
    experience: {
      uz: `7 YEARS`,
      ru: `7 YEARS`,
      en: `7 YEARS`,
    },
    desc: {
      uz: "I love teaching and helping my students be their best. I'm addicted to table tennis and reading. Acheivements of my students are a result of my strictness and professionalism.",
      ru: "I love teaching and helping my students be their best. I'm addicted to table tennis and reading. Acheivements of my students are a result of my strictness and professionalism.",
      en: "I love teaching and helping my students be their best. I'm addicted to table tennis and reading. Acheivements of my students are a result of my strictness and professionalism.",
    },
    score: "8.0",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Ms.Sandra1.png"),
    name: {
      uz: "Ms. Sandra",
      ru: "Ms. Sandra",
      en: "Ms. Sandra",
    },
    experience: {
      uz: `8 YEARS`,
      ru: `8 YEARS`,
      en: `8 YEARS`,
    },
    desc: {
      uz: "I am a qualified and an experienced IELTS teacher. Besides reading books and psychology, I also enjoy teaching students to communicate in English fluently. I love reading books and psychology and enjoy teaching students to communicate in English fluently.",
      ru: "I am a qualified and an experienced IELTS teacher. Besides reading books and psychology, I also enjoy teaching students to communicate in English fluently. I love reading books and psychology and enjoy teaching students to communicate in English fluently.",
      en: "I am a qualified and an experienced IELTS teacher. Besides reading books and psychology, I also enjoy teaching students to communicate in English fluently. I love reading books and psychology and enjoy teaching students to communicate in English fluently.",
    },
    score: "8.5",
    students: "2000+",
  },
  {
    image: require("~/assets/images/teachers/Mr.Abubakr1.png"),
    name: {
      uz: "Mr. Soliyev",
      ru: "Mr. Soliyev",
      en: "Mr. Soliyev",
    },
    experience: {
      uz: `5 YEARS`,
      ru: `5 YEARS`,
      en: `5 YEARS`,
    },
    desc: {
      uz: "Have you ever wanted to learn English as a second language but been too shy to try? I am an IELTS teacher who is passionate about teaching and learning. I am also a keen footballer who loves playing with my friends. I provide an enjoyable and a friendly for you to start your journey.",
      ru: "Have you ever wanted to learn English as a second language but been too shy to try? I am an IELTS teacher who is passionate about teaching and learning. I am also a keen footballer who loves playing with my friends. I provide an enjoyable and a friendly for you to start your journey.",
      en: "Have you ever wanted to learn English as a second language but been too shy to try? I am an IELTS teacher who is passionate about teaching and learning. I am also a keen footballer who loves playing with my friends. I provide an enjoyable and a friendly for you to start your journey.",
    },
    score: "8.0",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Mr.Sardor1.png"),
    name: {
      uz: "Mr. Erkinov",
      ru: "Mr. Erkinov",
      en: "Mr. Erkinov",
    },
    experience: {
      uz: `5 YEARS`,
      ru: `5 YEARS`,
      en: `5 YEARS`,
    },
    desc: {
      uz: "For students who want to achieve the highest results in their IELTS exam, and particularly improve their writing skills. Mr. Sardor has graduated Westminster International University in Tashkent, and has recieved training in several countries, including the USA.",
      ru: "For students who want to achieve the highest results in their IELTS exam, and particularly improve their writing skills. Mr. Sardor has graduated Westminster International University in Tashkent, and has recieved training in several countries, including the USA.",
      en: "For students who want to achieve the highest results in their IELTS exam, and particularly improve their writing skills. Mr. Sardor has graduated Westminster International University in Tashkent, and has recieved training in several countries, including the USA.",
    },
    score: "8.0",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Ms.Leyli.png"),
    name: {
      uz: "Ms. Mavlanova ",
      ru: "Ms. Mavlanova ",
      en: "Ms. Mavlanova ",
    },
    experience: {
      uz: `15 YEARS`,
      ru: `15 YEARS`,
      en: `15 YEARS`,
    },
    desc: {
      uz: "Dedicated to the pursuit of excellence. When I'm not teaching or playing the piano, I'm probably reading.",
      ru: "Dedicated to the pursuit of excellence. When I'm not teaching or playing the piano, I'm probably reading.",
      en: "Dedicated to the pursuit of excellence. When I'm not teaching or playing the piano, I'm probably reading.",
    },
    score: "7.5",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Mr.Ulugbek.png"),
    name: {
      uz: "Mr. Usmonov",
      ru: "Mr. Usmonov",
      en: "Mr. Usmonov",
    },
    experience: {
      uz: `4 YEARS`,
      ru: `4 YEARS`,
      en: `4 YEARS`,
    },
    desc: {
      uz: "Mr Ulugbek is for those who want to improve their English to perfection. The noteworthy feature of this teacher is that he can teach students how to express their own thoughts and fluently speak English in a short time: “With languages, you feel at home anywhere”",
      ru: "Mr Ulugbek is for those who want to improve their English to perfection. The noteworthy feature of this teacher is that he can teach students how to express their own thoughts and fluently speak English in a short time: “With languages, you feel at home anywhere”",
      en: "Mr Ulugbek is for those who want to improve their English to perfection. The noteworthy feature of this teacher is that he can teach students how to express their own thoughts and fluently speak English in a short time: “With languages, you feel at home anywhere”",
    },
    score: "7.5",
    students: "1000+",
  },
  {
    image: require("~/assets/images/teachers/Ms.Olya.png"),
    name: {
      uz: "Ms. Yazichenko",
      ru: "Ms. Yazichenko",
      en: "Ms. Yazichenko",
    },
    experience: {
      uz: `5 YEARS`,
      ru: `5 YEARS`,
      en: `5 YEARS`,
    },
    desc: {
      uz: "Goal-oriented teacher, dedicated to constant development of professional experience and students' results. Looking for entertaining and still knowledge-based lessons? Join and enjoy the atmosphere of my classes",
      ru: "Goal-oriented teacher, dedicated to constant development of professional experience and students' results. Looking for entertaining and still knowledge-based lessons? Join and enjoy the atmosphere of my classes",
      en: "Goal-oriented teacher, dedicated to constant development of professional experience and students' results. Looking for entertaining and still knowledge-based lessons? Join and enjoy the atmosphere of my classes",
    },
    score: "8.0",
    students: "1000+",
  },
];
