export default [
  {
    icon: "icon-We-are-Experienced",
    title: {
      en: "Our experience",
      uz: "Biz Tajribalimiz",
      ru: "Наш опыт",
    },
    subtitle: {
      en: "8 Years of Experience in the Market of Uzbekistan",
      uz: "8 yildan ortiq O’zbekiston bozorida tajribaga egamiz",
      ru: "Мы на рынке Узбекистана уже более 8 лет",
    },
  },
  {
    icon: "icon-Strong-Teachers",
    title: {
      en: "Experienced teachers ",
      uz: "Tajribali ustozlar",
      ru: "Опытные учителя",
    },
    subtitle: {
      en: "We have teachers with IELTS scores up to 9.0",
      uz: "Bizda IELTS darajasi 9.0 gacha bo’lgan ustozlar bor",
      ru: "У нас есть учителя с IELTS до 9.0",
    },
  },
  {
    icon: "icon-Great-Locations",
    title: {
      en: "Great Locations",
      uz: "Filiallar",
      ru: "Отличные локации",
    },
    subtitle: {
      en: "We have 9 Branches in Tashkent",
      uz: "Bizning Toshkent shahrida 9ta filialimiz bor",
      ru: "У нас 9 филиалов в городе Ташкент",
    },
  },
  {
    icon: "icon-Secured-Examination",
    title: {
      en: "Reliable exams",
      uz: "Ishonchli imtihon olish tizimi",
      ru: "Надёжные экзамены",
    },
    subtitle: {
      en: "Exams are conducted by a special exam invigilator",
      uz: "Imtihonlar maxsus nazoratchi tomonidan olib boriladi",
      ru: "У нас экзамены проводятся отвественными наблюдателями",
    },
  },
  {
    icon: "icon-British-Curriculum",
    title: {
      en: "British education system",
      uz: "Buyuk Britaniya ta’lim tizimi.",
      ru: "Британская система обучения",
    },
    subtitle: {
      en: "Our courses are based on materials by Oxford University Press",
      uz: "Bizda ta’lim Oxford University Press tizimiga asoslangan",
      ru: "Наши курсы основаны на системе обучения Oxford University Press",
    },
  },
  {
    icon: "icon-Award-Winner",
    title: {
      en: "Our achievements",
      uz: "Bizning yutuqlar",
      ru: "Наши награды",
    },
    subtitle: {
      en: "Cambridge is the winner of 2019-2020 'Brand of the Year' award",
      uz: `Cambridge 2019-2020 "Yil brendi" mukofoti g'olibi`,
      ru: "Cambridge — победитель премии «Бренд года 2019–2020» ",
    },
  },
  {
    icon: "icon-Free-Support",
    title: {
      en: "Free classes with support teachers. ",
      uz: "Qo'shimcha ustoz bilan bepul darslar",
      ru: "Бесплатные уроки с дополнительным учителем",
    },
    subtitle: {
      en: "Our support teachers are always happy to help you",
      uz: "Bizning qo’shimcha ustozlarimiz sizga yordam berishga doim tayyor",
      ru: "Наши дополнительные учителя всегда рады вам помочь",
    },
  },
  {
    icon: "icon-Free-Coworking-Areas",
    title: {
      en: "Free co-working zones.",
      uz: "Bepul co-working hududlar",
      ru: "Бесплатные коворкинг зоны",
    },
    subtitle: {
      en: "Cambridge provides you with the best study space free of charge.",
      uz: "Cambridge siz uchun eng qulay co-working hududlar yaratgan",
      ru: "Cambridge предоставляет вам лучшие бесплатные co-working зоны",
    },
  },
  {
    icon: "icon-Free-Events",
    title: {
      en: "Free Events",
      uz: "Bepul tadbirlar",
      ru: "Бесплатные мероприятия",
    },
    subtitle: {
      en: "Free Workshops, Speaking Clubs and Trainings every Sunday",
      uz: "Har yakshanba bepul seminarlar, Speaking klublari, treninglar",
      ru: "Бесплатные семинары, разговорные клубы, тренинги каждое воскресенье",
    },
  },
];
