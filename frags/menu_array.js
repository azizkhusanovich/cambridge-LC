export default [{
  text: {
    en: 'Courses',
    uz: 'Kurslar',
    ru: 'Курсы',
  },
  path: '#courses',
},
{
  text: {
    en: 'Team',
    uz: 'Jamoa ',
    ru: 'Команда',
  },
  path: '#team',
},

{
  text: {
    en: 'E-Platform',
    uz: 'Elektron Platforma',
    ru: 'Электронная платформа',
  },
  path: '#platform',
},
];
