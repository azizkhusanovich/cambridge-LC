export default [
    {
        icon: require('../assets/images/png/card1.svg'),
        title: {
            uz: 'IELTS raketasi',
            ru: 'IELTS Rocket',
            en: 'IELTS Rocket',
        },
        subtitle: {
            uz: "2 oyda IELTS dan kamida 6 yoki undan yuqori ball olishni xohlovchilar uchun",
            ru: 'Для тех, кто хочет получить не менее 6 баллов по IELTS за 2 месяца',
            en: 'For those who want to get at  least 6 or higher in IELTS after 2 months ',
        },
    },
    {
        icon: require('../assets/images/png/card2.svg'),
        card_link: require('../assets/images/png/card11.png'),

        title: {
            uz: 'Umumiy ingliz',
            ru: 'Общий английский',
            en: 'General English',
        },
        subtitle: {
            uz: "4 oydan keyin IELTS imtihonini topshirib, yuqori ball olishni xohlovchilar uchun",
            ru: 'Для тех, кто хочет сдать экзамен IELTS через 4 месяца и получить высокий балл',
            en: 'For those who want to learn English from scratch or from a certain level',
        },
    },
    {
        icon: require('../assets/images/png/Vector.svg'),
        card_link: require('../assets/images/png/card22.png'),

        title: {
            uz: "IELTS to'liq kursi",
            ru: 'Полный курс IELTS',
            en: 'IELTS Full Course',
        },
        subtitle: {
            uz: "2 oydan keyin IELTS dan kamida 6 yoki undan yuqori ball olishni xohlovchilar uchun",
            ru: 'Для тех, кто хочет получить не менее 6 баллов по IELTS через 2 месяца',
            en: 'For those who want to take the IELTS exam after 4 months and get a high score',
        },
    },
    {
        icon: require('../assets/images/png/card4.svg'),
        card_link: require('../assets/images/png/card33.png'),

        title: {
            uz: "Individual va Mini Guruhlar",
            ru: 'Индивидуальный и Мини Группы',
            en: 'Individual & Mini Groups',
        },
        subtitle: {
            uz: "Ingliz tilini yakka tartibda yoki 2, 3 yoki 4 kishilik mini guruhlarda o'rganmoqchi bo'lganlar uchun ",
            ru: 'Для тех, кто хочет изучать английский язык индивидуально или в мини-группах по 2, 3 или 4 человека.',
            en: 'For those who want to learn English individually or in mini groups of 2, 3, or 4 people',
        },
    },
]