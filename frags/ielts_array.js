export default [
  {
    img: require('~/assets/images/ielts/ozoda_d.png'),
    background: "linear-gradient(87.72deg, #131313 2.12%, #545455 98.98%)",
    alt: 'Cambridge IELTS результаты – Dilnura',
    name: "Ozodova",
    surname: "Dilnura",
    listening: "8.0",
    reading: "8.0",
    writing: "6.0",
    speaking: "8.0",
    bandscore: "7.5"
  },
  {
    img: require('~/assets/images/ielts/baxromova_n.png'),
    background: "linear-gradient(87.72deg, #F48635 2.12%, #FFCB2A 98.98%)",
    alt: 'Cambridge IELTS результаты – Nozimaxon',
    name: "Baxromova",
    surname: "Nozimaxon",
    listening: "7.5",
    reading: "8.5",
    writing: "6.0",
    speaking: "7.0",
    bandscore: "7.5"
  },
  {
    img: require('~/assets/images/ielts/ss.png'),
    background: "linear-gradient(87.72deg, #DA4540 2.12%, #E58447 98.98%)",
    alt: 'Cambridge IELTS результаты - Sadriddinkhuja',
    name: "Salokhiddinov",
    surname: "Sadriddinkhuja",
    listening: "8.5",
    reading: "8.0",
    writing: "6.5",
    speaking: "7.0",
    bandscore: "7.5"
  },

  {
    img: require('~/assets/images/ielts/axmetov_m.png'),
    background: "linear-gradient(87.72deg, #634382 2.12%, #7A5492 98.98%)",
    alt: 'Cambridge IELTS результаты - Muzaffar',
    name: "Axmetov",
    surname: "Muzaffar",
    listening: "9.0",
    reading: "8.5",
    writing: "6.5",
    speaking: "7.5",
    bandscore: "8.0"
  },
  {
    img: require('~/assets/images/ielts/nurseitov_m.png'),
    background: "linear-gradient(87.72deg, #00A360 2.12%, #67B35F 98.98%)",
    alt: 'Cambridge IELTS результаты - Mirzabek',
    name: "Nurseitov",
    surname: "Mirzabek",
    listening: "9.0",
    reading: "8.0",
    writing: "7.0",
    speaking: "7.0",
    bandscore: "8.0"
  },
  {
    img: require('~/assets/images/ielts/muradovF.png'),
    background: "linear-gradient(87.72deg, #314085 2.12%, #00A6E8 98.98%)",
    alt: 'Cambridge IELTS результаты – Farida',
    name: "Muradova",
    surname: "Farida",
    listening: "9.0",
    reading: "8.0",
    writing: "7.5",
    speaking: "8.5",
    bandscore: "8.5"
  },
]