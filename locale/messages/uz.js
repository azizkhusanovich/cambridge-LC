const uz = {
  Ofetra: "Oferta",
  Pay_Online: "Onlayn to'lash",
  Pay: "To'lov",
  Personal_Area: "Shaxsiy kabinet",
  Personal: "Kabinetga kirish",
  secBanner: {
    title: `<h2>Biz nafaqat <span>ingliz tilini o'rgatamiz,</span>  balki <span>insonlar hayotini o'zgartiramiz!</span></h2>`,
    subtitle: `<span>Kembrij o‘quv markazida</span> 8 yil davomida <span>45 000 dan ortiq</span>  kishi tahsil oldi`,
    subtitle: `8 yil ichida <span> Cambridge’da 45000 dan ortiq</span> inson o’qigan`,
    registerTitle: "Birinchi darsga yozilish",
    sign_title: "Kabinetga kirish",
    player_title: "Cambridge haqida 2 daqiqali video",
  },
  secChoose: {
    title: "Nima uchun aynan bizni tanlashingiz kerak?",
    subtitle: "Talabalarimizning har biri quyidagi afzalliklarga ega bo'ladi",
  },
  secPlatform: {
    title: "Bizdagi gibrid o’qitish tizimi",
    subtitle:
      "Tezroq va yaxshiroq o'rganishni rag'batlantirish uchun har bir darajadagi talabalar uchun moslashtirilgan onlayn platforma",
    sign_title: "Kabinetga kirish",
    videoText: "Tizim haqidagi videoni tomosha qiling",
    people_title: "Munisa Eshova",
    people_subtitle: "40 bal oldi",
    people_title2: "Saida Kamolova",
    people_subtitle2: "20 bal oldi",
  },
  secStudentsSlider: {
    title: "Bizning o’quvchilarimiz",
    subtitle: "Bizning o’quvchilarimiz quyidagi yutuqlarga erishgan",
  },
  secteachers: {
    title: `<h2>Bizning <span>ustozlar</span></h2> `,
  },
  secChoice: {
    title: "Sizga mos keladigan kursni tanlang",
    sign_up: "Ro'yxatdan o'tish",
  },
  secLocation: {
    title: "Toshkent shahridagi filiallarimiz",
  },
  footer: {
    learn: "Ko'proq ma'lumot olish",
    contact_Us: "Biz bilan bog’laning",
    phone: "Telefon",
    social: "Ijtimoiy Tar.",
  },
  registration: {
    title: "O’zbekistondagi ILK Gibrid Ta’lim til markazi!",
    subtitle:
      "O'quv markazimizga siz ham qo’shiling va bizning yangi Gibrid Ta’lim tizimi bilan yuqori natijalarga erishing!",
    form_title: `Ro'yxatdan o'tish`,
    name: "Ismingiz",
    surename: " Familiyangiz",
    phone: " Telefon raqamingiz",
    button: "Yuborish ",
  },
  thanks: `Ma'lumotlaringiz qabul qilindi, tez fursatlarda biz siz bilan bo'glanamiz`,
  errors: {
    text: `Iltimos ushbu maydonni to'ldiring`,
    phone: `Telefon raqamini 991234567 formatda yozing`,
    post: `Qandaydir xatolik yuz berdi.`,
    message: `Siz allaqachon ro'yxatdan o'tgansiz, biz siz bilan eng tez fursatlarda bog'lanamiz`,
  },
  home: "Bosh sahifa",
};

export default uz;
