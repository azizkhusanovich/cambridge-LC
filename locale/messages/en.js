const en = {
  Ofetra: "Offer",
  Pay_Online: "Pay Online",
  Pay: "Pay",
  Personal_Area: "Personal Area",
  Personal: "Log in",
  secBanner: {
    title: `<h2>We don’t just <span>teach English,</span> <br> we <span>change lives!</span></h2> `,
    subtitle: `In 8 years, more than <span>45,000 people</span> have studied at the <span>Cambridge Learning Center</span> `,
    registerTitle: "Register to first class",
    sign_title: "Log in",
    player_title: "Learn about Cambridge in 2 minutes",
  },
  secChoose: {
    title: "Why To Choose Cambridge",
    subtitle: "Each of our students will have the following advantages",
  },
  secPlatform: {
    title: "We have a hybrid learning system. ",
    subtitle:
      "Customized online platform for each level of students to encourage faster and better learning.",
    sign_title: "Log in",
    videoText: "Learn more about the system",
    people_title: "Munisa Eshova",
    people_subtitle: "Earned 40 points",
    people_title2: "Saida Kamolova",
    people_subtitle2: "Earned 20 points",
  },
  secStudentsSlider: {
    title: "Our Students",
    subtitle: "Our students have achieved the following results.",
  },
  secteachers: {
    title: `<h2>Meet the <span>TEAM.</span></h2>`,
  },
  secChoice: {
    title: "Choose the course which fits you",
    sign_up: "Sign up",
  },
  secLocation: {
    title: "Our branches in Tashkent",
  },
  registration: {
    title: "The first Hybrid Education Language Center in Uzbekistan!",
    subtitle:
      "Join our training center and achieve high results with our new Hybrid Education system!",
    form_title: "Registration",
    name: "First Name",
    surename: "Last Name",
    phone: "Phone number",
    button: "Send",
  },
  errors: {
    text: `Please fill in this field`,
    phone: `Enter the phone number in the format 991234567`,
    post: `Something went wrong`,
    message: `You are already registered, we will contact you as soon as possible`,
  },
  thanks: `Your information has been received, we will contact you shortly`,
  footer: {
    learn: "Learn More",
    contact_Us: "Contact Us",
    phone: "Phone",
    social: "Social",
  },
  home: 'Main page'
};

export default en;
